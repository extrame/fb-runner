# Fabric_Runner

## 介绍
用于替换Fabric Test Network的测试工具

### 注意

本工具不能兼容fabric-sample/test-network，需要重新配置

## 软件架构
软件架构说明


## 安装教程

1. go get gitee.com/extrame/fb-runner

## 使用说明

1.  离线下载用于Fabric安装需要使用的Docker镜像
2.  创建一个类似于Fabric Sample的Test Network的Fabric使用环境
3.  提供用于安装和调用链上合约的脚本工具

### 子命令

#### 已支持

##### make_install

```sh
fb-runner make_install
```

本命令在具有网络连接能力的设备上执行，可以生成所有Fabric需要的Docker镜像，无需安装Docker

##### install

```
fb-runner install <file>
```

本命令将安装fabric打包的程序包，程序包用file参数指定

##### sync_installed

```
fb-runner sync_installed
```

本命令用于同步所有链上已安装的程序，同名程序将作为系列管理，也就是新提交的同名程序将作为旧程序的升级包对待，fb-runner将询问是否将新的提交程序作为旧程序的历史版本（忽略）或者作为新版本（作为更新）

##### mark_latest

```
fb-runner mark_latest <package_id>
```

本命令用于将某一个版本的同名程序作为最新版本使用，传入参数为对应版本的package_id

##### approve

```
fb-runner approve
```

本命令用于在所有链上机构中批准安装的链码，批准过程会询问用户使用的版本标签等信息，程序自动根据历史版本信息生成新的版本号，用户可选择使用或不使用，例如上一批准版本为v1.0.1,将自动选择v1.0.2

##### commit

```
fb-runner commit
```

本命令用于提交批准的链码

##### invoke

```
fb-runner invoke
```

本命令用于调用已批准的链码

##### env

```
export $(fb-runner env org1/org2)
```

本命令用于在命令行中注入对应机构名称所需的相关ENV参数，例如FABRIC_CFG_PATH等，用户在使用该命令后，可正常使用Fabric peer等命令

##### export_logs

```
fb-runner export_logs
```

可选参数：--recreate （重新从创建时间起获得日志）

获得从上次获得之后的新日志，比如上次获得到2021年10月1日 10:00 的日志，本次将继续获得之后的数据，直到没有新日志或者直到当前时间为止。

本命令可通过配置文件logfile更改目标文件名称，否则为base路径下的log-fabric-${项目名称}文件

## 开发计划

计划后续支持以下特性：

- [x] 自定义机构名称
- [x] 多机部署支持
- [ ] 欢迎补充

## Bug修正计划

- [ ] 从机的extra_hosts没有生成
- [ ] 从机没有ca时的down配置错误
- [ ] 欢迎补充

