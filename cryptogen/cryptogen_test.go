package cryptogen

import (
	"fmt"
	"testing"

	"gitee.com/extrame/fb-runner/util"
)

func TestGenerateTemp(t *testing.T) {
	util.Config.Init()
	var org = util.Config.Organizations["org1"]
	util.Mkdir("../test/result/crypto")
	cfg, err := generateTempCryptogenForOrg(org, "../samples/cryptogen/crypto-config-peer.yaml", "../test/result/crypto")
	fmt.Println(cfg, err)
}

func TestGenerateTempForOrder(t *testing.T) {
	util.Config.Init()
	var org = util.Config.Organizations["ordererOrg"]
	util.Mkdir("../test/result/crypto")
	cfg, err := generateTempCryptogenForOrg(org, "../samples/cryptogen/crypto-config-orderer.yaml", "../test/result/crypto")
	fmt.Println(cfg, err)
}
