package main

import (
	"fmt"
	"os"
	"path/filepath"
)

func printHelp(cmd string) {
	switch cmd {
	case "up":
		fmt.Println("Usage: ")
		fmt.Println("  runner \033[0;32mup\033[0m [Flags]")
		fmt.Println()
		fmt.Println("    Flags:")
		fmt.Println("    -ca <use CAs> -  Use Certificate Authorities to generate network crypto material")
		fmt.Println("    -c <channel name> - Name of channel to create (defaults to \"mychannel\")")
		fmt.Println("    -s <dbtype> - Peer state database to deploy: goleveldb (default) or couchdb")
		fmt.Println("    -r <max retry> - CLI times out after certain number of attempts (defaults to 5)")
		fmt.Println("    -d <delay> - CLI delays for a certain number of seconds (defaults to 3)")
		fmt.Println("    -verbose - Verbose mode")
		fmt.Println()
		fmt.Println("    -h - Print this message")
		fmt.Println()
		fmt.Println(" Possible Mode and flag combinations")
		fmt.Println("   \033[0;32mup\033[0m -ca -r -d -s -verbose")
		fmt.Println("   \033[0;32mup createChannel\033[0m -ca -c -r -d -s -verbose")
		fmt.Println()
		fmt.Println(" Examples:")
		fmt.Println("   runner up createChannel -ca -c mychannel -s couchdb ")
	default:
		fmt.Println("Usage: ")
		fmt.Println("  runner <Mode> [Flags]")
		fmt.Println("    Modes:")
		fmt.Println("      \033[0;32mup\033[0m - Bring up Fabric orderer and peer nodes. No channel is created")
		fmt.Println("      \033[0;32mup createChannel\033[0m - Bring up fabric network with one channel")
		fmt.Println("      \033[0;32mcreateChannel\033[0m - Create and join a channel after the network is created")
		fmt.Println("      \033[0;32mdeployCC\033[0m - Deploy a chaincode to a channel (defaults to asset-transfer-basic)")
		fmt.Println("      \033[0;32mdown\033[0m - Bring down the network")
		fmt.Println()
		fmt.Println("    Flags:")
		fmt.Println("    Used with \033[0;32mrunner up\033[0m, \033[0;32mrunner createChannel\033[0m:")
		fmt.Println("    -ca <use CAs> -  Use Certificate Authorities to generate network crypto material")
		fmt.Println("    -c <channel name> - Name of channel to create (defaults to \"mychannel\")")
		fmt.Println("    -s <dbtype> - Peer state database to deploy: goleveldb (default) or couchdb")
		fmt.Println("    -r <max retry> - CLI times out after certain number of attempts (defaults to 5)")
		fmt.Println("    -d <delay> - CLI delays for a certain number of seconds (defaults to 3)")
		fmt.Println("    -verbose - Verbose mode")
		fmt.Println()
		fmt.Println("    Used with \033[0;32mrunner deployCC\033[0m")
		fmt.Println("    -c <channel name> - Name of channel to deploy chaincode to")
		fmt.Println("    -ccn <name> - Chaincode name.")
		fmt.Println("    -ccl <language> - Programming language of the chaincode to deploy: go, java, javascript, typescript")
		fmt.Println("    -ccv <version>  - Chaincode version. 1.0 (default), v2, version3.x, etc")
		fmt.Println("    -ccs <sequence>  - Chaincode definition sequence. Must be an integer, 1 (default), 2, 3, etc")
		fmt.Println("    -ccp <path>  - File path to the chaincode.")
		fmt.Println("    -ccep <policy>  - (Optional) Chaincode endorsement policy using signature policy syntax. The default policy requires an endorsement from Org1 and Org2")
		fmt.Println("    -cccg <collection-config>  - (Optional) File path to private data collections configuration file")
		fmt.Println("    -cci <fcn name>  - (Optional) Name of chaincode initialization function. When a function is provided, the execution of init will be requested and the function will be invoked.")
		fmt.Println()
		fmt.Println("    -h - Print this message")
		fmt.Println()
		fmt.Println(" Possible Mode and flag combinations")
		fmt.Println("   \033[0;32mup\033[0m -ca -r -d -s -verbose")
		fmt.Println("   \033[0;32mup createChannel\033[0m -ca -c -r -d -s -verbose")
		fmt.Println("   \033[0;32mcreateChannel\033[0m -c -r -d -verbose")
		fmt.Println("   \033[0;32mdeployCC\033[0m -ccn -ccl -ccv -ccs -ccp -cci -r -d -verbose")
		fmt.Println()
		fmt.Println(" Examples:")
		fmt.Println("   runner up createChannel -ca -c mychannel -s couchdb")
		fmt.Println("   runner createChannel -c channelName")
		fmt.Println("   runner deployCC -ccn basic -ccp ../asset-transfer-basic/chaincode-javascript/ -ccl javascript")
		fmt.Println("   runner deployCC -ccn mychaincode -ccp ./user/mychaincode -ccv 1 -ccl javascript")
	}
	// if [ "$USAGE" == "up" ]; then

	// elif [ "$USAGE" == "createChannel" ]; then
	//   println "Usage: "
	//   println "  runner \033[0;32mcreateChannel\033[0m [Flags]"
	//   println
	//   println "    Flags:"
	//   println "    -c <channel name> - Name of channel to create (defaults to \"mychannel\")"
	//   println "    -r <max retry> - CLI times out after certain number of attempts (defaults to 5)"
	//   println "    -d <delay> - CLI delays for a certain number of seconds (defaults to 3)"
	//   println "    -verbose - Verbose mode"
	//   println
	//   println "    -h - Print this message"
	//   println
	//   println " Possible Mode and flag combinations"
	//   println "   \033[0;32mcreateChannel\033[0m -c -r -d -verbose"
	//   println
	//   println " Examples:"
	//   println "   runner createChannel -c channelName"
	// elif [ "$USAGE" == "deployCC" ]; then
	//   println "Usage: "
	//   println "  runner \033[0;32mdeployCC\033[0m [Flags]"
	//   println
	//   println "    Flags:"
	//   println "    -c <channel name> - Name of channel to deploy chaincode to"
	//   println "    -ccn <name> - Chaincode name."
	//   println "    -ccl <language> - Programming language of chaincode to deploy: go, java, javascript, typescript"
	//   println "    -ccv <version>  - Chaincode version. 1.0 (default), v2, version3.x, etc"
	//   println "    -ccs <sequence>  - Chaincode definition sequence. Must be an integer, 1 (default), 2, 3, etc"
	//   println "    -ccp <path>  - File path to the chaincode."
	//   println "    -ccep <policy>  - (Optional) Chaincode endorsement policy using signature policy syntax. The default policy requires an endorsement from Org1 and Org2"
	//   println "    -cccg <collection-config>  - (Optional) File path to private data collections configuration file"
	//   println "    -cci <fcn name>  - (Optional) Name of chaincode initialization function. When a function is provided, the execution of init will be requested and the function will be invoked."
	//   println
	//   println "    -h - Print this message"
	//   println
	//   println " Possible Mode and flag combinations"
	//   println "   \033[0;32mdeployCC\033[0m -ccn -ccl -ccv -ccs -ccp -cci -r -d -verbose"
	//   println
	//   println " Examples:"
	//   println "   runner deployCC -ccn basic -ccp ../asset-transfer-basic/chaincode-javascript/ ./ -ccl javascript"
	//   println "   runner deployCC -ccn mychaincode -ccp ./user/mychaincode -ccv 1 -ccl javascript"
	// else

	// fi
}

func isExists(path string) bool {
	path = filepath.FromSlash(path)
	_, err := os.Stat(path)
	return err == nil
}
