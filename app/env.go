package app

import (
	"fmt"
	"path/filepath"

	"gitee.com/extrame/fb-runner/util"
)

func SetEnv(base string, orgName string) error {
	//peer lifecycle chaincode install ~/anhui1.tar.gz
	if org, ok := util.Config.Organizations[orgName]; ok {
		cfgDir := filepath.Join(base, "config")
		p, err := org.GetLocalPeer()
		if err == nil {
			caPath := filepath.Join(base, "organizations", org.GetParentDir(), org.GetHost()+"/peers/"+p.GetHost()+"/tls/ca.crt")
			mspPath := filepath.Join(base, "organizations", org.GetParentDir(), org.GetHost()+"/users/Admin@"+org.GetHost()+"/msp")
			envs := []string{
				"FABRIC_CFG_PATH=" + cfgDir,
				"CORE_PEER_TLS_ENABLED=true",
				"CORE_PEER_LOCALMSPID=" + org.Msp,
				"CORE_PEER_TLS_ROOTCERT_FILE=" + caPath,
				"CORE_PEER_MSPCONFIGPATH=" + mspPath,
				"CORE_PEER_ADDRESS=" + p.GetUrl(),
			}
			for _, env := range envs {
				fmt.Println(env)
			}
		} else {
			return err
		}
	}
	return nil
}
