package app

import (
	"encoding/json"
	"fmt"
	"testing"
)

func TestUnmarshal(t *testing.T) {
	var jsonS = `{
		"installed_chaincodes": [
			{
				"package_id": "anhui-data-record-v1:62e3700116e9e8fcfba8255f64f2ec05dfbe1965acfbc47e871fc938ad0c96c4",
				"label": "anhui-data-record-v1"
			},
			{
				"package_id": "anhui-data-record-v1:d00379ed3426fc0f350945d5278401597c6b101211f249064bfb7127553743ee",
				"label": "anhui-data-record-v1",
				"references": {
					"anhuiceshi": {
						"chaincodes": [
							{
								"name": "anhuidata",
								"version": "1.1.3"
							}
						]
					}
				}
			},
			{
				"package_id": "anhui-data-record-v1:bece61153f394c2d8b2c4bc619fea1dcf2dc1e8d60eb29d735556a056ed9b77c",
				"label": "anhui-data-record-v1"
			},
			{
				"package_id": "anhui-data-record-v1:0b9f70266508c93ad8866d6206aa0df6372dc6b380f66974fb0953d494e2e584",
				"label": "anhui-data-record-v1"
			},
			{
				"package_id": "anhui-data-record-v1:0d007ad834354aa71da87f551a0f1668f3f398387c5cd34a8d8af6b2ab6bc72d",
				"label": "anhui-data-record-v1"
			},
			{
				"package_id": "lishui-data-record-v4:a371486cee9dac65019bed25b0d58218f104caddf2e6dd9032f33fe5f91b484b",
				"label": "lishui-data-record-v4"
			},
			{
				"package_id": "lishui-data-record-v6:4af5d957b5ffa09840725ca26730621e5b1d8f5bad53de946dd68fa6ac2d605f",
				"label": "lishui-data-record-v6",
				"references": {
					"puhuijinrong": {
						"chaincodes": [
							{
								"name": "lishuidata",
								"version": "1.0.2"
							}
						]
					}
				}
			},
			{
				"package_id": "anhui-data-record-v1:f79d73c17d9942bf158d7f78875485147365599e5e6a88fde3f0772c6e56134f",
				"label": "anhui-data-record-v1"
			},
			{
				"package_id": "anhui-data-record-v1:eef779b63f8cd89563d2743be04fe66d43b218ad8652dbe7f323e9b2a95c2927",
				"label": "anhui-data-record-v1"
			},
			{
				"package_id": "anhui-data-record-v1:e7b8b97116e42e9c0e8f4b5c23e27690a430ae95d8e8e64a3a062e7b4b97f2c3",
				"label": "anhui-data-record-v1"
			},
			{
				"package_id": "anhui-data-record-v1:a1e43d7384a2b718d54f6687b9c2ec8ba9b1787d9ff7ee90c3ba21f4f8a00b7f",
				"label": "anhui-data-record-v1"
			},
			{
				"package_id": "lishui-data-record-v6:bb25b206156715ac9de1a8467a1d9684f1638780d128f2aa061f2e69352663c3",
				"label": "lishui-data-record-v6"
			},
			{
				"package_id": "anhui-data-record-v1:83912c5a99d86843b056bb8018013bb7352fbc890ec76eb3018efe34580ce0ac",
				"label": "anhui-data-record-v1"
			},
			{
				"package_id": "anhui-data-record-v1:71f4adfcb80c2303d51c688b2bd4b176b10c8bcc8191a8164f1012ba0ed9e1a0",
				"label": "anhui-data-record-v1"
			},
			{
				"package_id": "anhui-data-record-v1:e5d7dea12cff8df3584dbe6346401a21d433cbe73fdb65f67c8e412a20e2da5a",
				"label": "anhui-data-record-v1"
			},
			{
				"package_id": "lishui-opendata-v-test:09eb60c7ca194a3f65772d92cb20d27ac018fd713e37fc916067c8ccdd2c01cf",
				"label": "lishui-opendata-v-test"
			},
			{
				"package_id": "anhui-data-record-v1:c9b908422ef833c474580f60257b862d85788a38a5bbb939492b306acadd666a",
				"label": "anhui-data-record-v1"
			},
			{
				"package_id": "anhui-data-record-v1:c5ca9a470aff96b0012012375f2d9265f0c60aafdfa652de5e66fe334c9f97b4",
				"label": "anhui-data-record-v1"
			}
		]
	}`
	var out OutputJson
	err := json.Unmarshal([]byte(jsonS), &out)
	if err != nil {
		t.Fatal(err)
	}
	out.MarkHistoric()
	for _, code := range out.InstalledChaincodes {
		fmt.Println(code)
	}
}
