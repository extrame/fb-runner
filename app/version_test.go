package app

import (
	"fmt"
	"testing"
)

func TestFindVersion(t *testing.T) {
	var str string
	str = findVersion("v1.0.0")
	fmt.Println(str)
	str = findVersion("v1_0")
	fmt.Println(str)
	str = findVersion("1.0.1")
	fmt.Println(str)
	str = findVersion("v201223")
	fmt.Println(str)
	str = findVersion("v_final")
	fmt.Println(str)
}
