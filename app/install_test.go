package app

import (
	"encoding/json"
	"fmt"
	"testing"
)

func TestJsonOutput(t *testing.T) {
	var output = `
{
	"installed_chaincodes":[{
		"package_id":"test",
		"label":"test2"
	}]
}`
	var out OutputJson
	json.Unmarshal([]byte(output), &out)
	for _, c := range out.InstalledChaincodes {
		fmt.Println(c)
	}

}
