package channel

import (
	"os"
	"path/filepath"
	"text/template"

	"gitee.com/extrame/fb-runner/util"
)

func GenConfigTx(source, base string) error {

	t, err := template.New("configtx.yaml").Funcs(template.FuncMap{
		"isLast": func(i int, peers map[string]*util.Peer) bool {
			return i == len(peers)-1
		},
	}).ParseFiles(filepath.Join(source, "config/configtx.yaml"))
	if err == nil {
		var f *os.File
		f, err = os.OpenFile(filepath.Join(base, "config/configtx.yaml"), os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
		if err == nil {
			err = t.Execute(f, util.Config.Organizations)
			f.Close()
		}
	}

	return err
}
