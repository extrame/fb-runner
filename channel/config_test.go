package channel

import (
	"testing"

	"gitee.com/extrame/fb-runner/util"
)

func TestConfigTxGen(t *testing.T) {
	util.Config.Init()
	util.MkdirFor("../test/result/config/configtx.yaml")
	err := GenConfigTx("../samples", "../test/result")
	if err != nil {
		t.Fatal(err)
	}
}
