package store

import (
	"github.com/denisbrodbeck/machineid"
	"github.com/sirupsen/logrus"
)

var masterId = ""
var localId = ""

func GetLocalId() string {
	if localId == "" {
		var err error
		localId, err = machineid.ProtectedID("fb-runner")
		if err != nil {
			logrus.Fatalln(err)
		}
	}
	return localId
}

// func MarkMaster(reportIfNotMaster bool) error {
// 	// 先判断数据库中是否已经有主机器存在
// 	mId := getMasterId()
// 	id, err := machineid.ProtectedID("fb-runner")
// 	if err != nil {
// 		logrus.Fatal("获取本机ID失败", err)
// 	} else if mId != "" && mId != id && reportIfNotMaster {
// 		logrus.Fatalf("本机（%s）不是主机(%s)", id, mId)
// 	}
// 	logrus.Infoln("获得本级ID" + id)
// 	err = saveMasterId(id)
// 	return err
// }

// func IsMaster() bool {
// 	// 先判断数据库中是否已经有主机器存在
// 	mId := getMasterId()
// 	id, err := machineid.ProtectedID("fb-runner")
// 	if err != nil {
// 		logrus.Fatal("获取本机ID失败", err)
// 	} else if mId != "" && mId != id {
// 		return false
// 	}
// 	return true
// }

// func getMasterId() string {
// 	if masterId == "" {
// 		DB.View(func(tx *bolt.Tx) (err error) {
// 			var b *bolt.Bucket
// 			if b = tx.Bucket([]byte("config")); b != nil {
// 				bts := b.Get([]byte("master_id"))
// 				if bts != nil {
// 					masterId = string(bts)
// 				}
// 			}
// 			return
// 		})
// 	}
// 	return masterId
// }

// func saveMasterId(id string) error {
// 	return DB.Update(func(tx *bolt.Tx) (err error) {
// 		var b *bolt.Bucket
// 		b, err = tx.CreateBucketIfNotExists([]byte("config"))
// 		if err == nil {
// 			err = b.Put([]byte("master_id"), []byte(id))
// 		}
// 		return err
// 	})
// }
