package store

import (
	"encoding/binary"
	"path/filepath"

	"github.com/boltdb/bolt"
)

var DB *bolt.DB

func Init(base string) error {
	db, err := bolt.Open(filepath.Join(base, "fb-runner.db"), 0600, nil)
	if err != nil {
		return err
	}
	DB = db
	FSM.db = db

	DB.View(func(t *bolt.Tx) error {
		b := t.Bucket([]byte("config"))
		if b != nil {
			bts := b.Get([]byte("fsm"))
			if bts != nil {
				FSM.step = binary.BigEndian.Uint64(bts)
			}
		}
		return err
	})

	return nil
}
