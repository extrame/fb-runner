package store

import (
	"encoding/binary"
	"time"

	"github.com/boltdb/bolt"
)

//和日志存储相关的配置项的保存和读取

func GetLastExportDate() time.Time {
	var date time.Time
	DB.View(func(tx *bolt.Tx) (err error) {
		var b *bolt.Bucket
		if b = tx.Bucket([]byte("config")); b != nil {
			bts := b.Get([]byte("last_export_date"))
			if bts != nil {
				date = time.Unix(int64(binary.BigEndian.Uint64(bts)), 0)
			}
		}
		return
	})
	return date
}

func SetLastExportDate(date time.Time) error {
	return DB.Update(func(tx *bolt.Tx) (err error) {
		var b *bolt.Bucket
		if b, err = tx.CreateBucketIfNotExists([]byte("config")); err == nil {
			var bts = make([]byte, 8)
			binary.BigEndian.PutUint64(bts, uint64(date.Unix()))
			err = b.Put([]byte("last_export_date"), bts)
		}
		return
	})
}
