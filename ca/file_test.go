package ca

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestMatch(t *testing.T) {
	var src = "/Users/liuming/workspace/*"
	var realSrc string
	if strings.HasSuffix(src, "*") {
		dir := filepath.Dir(src)
		name := filepath.Base(src)
		filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
			fmt.Println(path, dir, name, info.Name())
			if !info.IsDir() {
				matched, _ := filepath.Match(name, info.Name())
				if matched {
					realSrc = path
					return errors.New("Found")
				}
			}
			return nil
		})
	} else {
		realSrc = src
	}
	fmt.Println(realSrc)
}
