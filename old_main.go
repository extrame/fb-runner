package main

// var cfg = util.Config

// var (
// 	forceRecreate = flag.Bool("force", false, "force to recreate organization config files")
// )

// func clearContainers() {
// 	logrus.Info("Removing remaining containers")
// 	docker.Remove(docker.FabricDockers)
// 	docker.Remove(docker.FabricDevPeers)
// }

// func removeUnwantedImages() {
// 	logrus.Info("Removing generated chaincode docker images")
// 	docker.RemoveUnwanted(docker.FabricDevPeers)
// }

// func clean() {
// 	clearContainers()
// 	removeUnwantedImages()

// }

// func main() {
// 	// # Parse commandline args
// 	var file string
// 	flag.StringVar(&file, "config", "runner.cfg", "config file")
// 	flag.Parse()

// 	f.Close()

// 	var args = flag.Args()

// 	if len(args) == 0 {
// 		printHelp("")
// 		os.Exit(0)
// 	}
// 	var cmd = args[0]

// 	err = docker.Init()

// 	if err == nil {
// 		err = cfg.CheckSource()
// 		if err != nil {
// 			logrus.Fatal(err.Error(), "请检查")
// 		}
// 		// ## Parse mode
// 		switch cmd {
// 		case "up":

// 			networkUp()
// 		case "down":

// 		case "clean":
// 			clean()
// 		case "create":

// 		case "make_install":
// 			err := docker.Install("hyperledger/fabric-baseos", cfg.Version)
// 			checkErr(err)
// 			err = docker.Install("hyperledger/fabric-ca", cfg.CaVersion)
// 			checkErr(err)
// 			err = docker.Install("hyperledger/fabric-ccenv", cfg.Version)
// 			checkErr(err)
// 			err = docker.Install("hyperledger/fabric-couchdb", "latest")
// 			checkErr(err)
// 			err = docker.Install("hyperledger/fabric-javaenv", cfg.Version)
// 			checkErr(err)
// 			err = docker.Install("hyperledger/fabric-kafka", "latest")
// 			checkErr(err)
// 			err = docker.Install("hyperledger/fabric-orderer", cfg.Version)
// 			checkErr(err)
// 			err = docker.Install("hyperledger/fabric-peer", cfg.Version)
// 			checkErr(err)
// 			err = docker.Install("hyperledger/fabric-tools", cfg.Version)
// 			checkErr(err)
// 			err = docker.Install("hyperledger/fabric-zookeeper", "latest")
// 			checkErr(err)
// 		case "install":

// 		case "sync_installed":

// 		case "mark_latest":

// 		case "approve":
//
// 		case "invoke":

// 		case "env":

// 		case "commit":

// 		case "update_anchor":
// 			err = channel.UpdateAnchorPeers(cfg.GetBase(), args[1])
// 			checkErr(err)
// 		case "services": //获取所有运行的docker名称v

// 		case "hostid":
//
// 		case "export_logs":

// 		default:
// 			printHelp(flag.Arg(1))
// 		}
// 		if err != nil {
// 			logrus.Fatalln(err)
// 		} else {
// 			logrus.Infoln("执行成功！")
// 		}
// 		return
// 	}

// }
