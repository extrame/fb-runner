package peer

import (
	"os"
	"os/exec"
	"path/filepath"

	"gitee.com/extrame/fb-runner/version"
)

func Version() (version.Version, error) {
	//  peer version > /dev/null 2>&1
	cmd := exec.Command("peer", "version")
	bts, err := cmd.Output()
	if err == nil {
		return version.New(string(bts)), nil
	}
	return version.New(""), err
}

func CheckConfig(cfg string) bool {
	cfg = filepath.FromSlash(cfg)
	_, err := os.Stat(cfg)
	return err == nil
}
