/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"gitee.com/extrame/fb-runner/app"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var commitCfg struct {
	Version *int64
	Force   *bool
}

// commitCmd represents the commit command
var commitCmd = &cobra.Command{
	Use:   "commit",
	Short: "提交已经批准的链码",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		var err error
		if len(args) > 0 {
			code := app.GetInstalledChaincode(args[0])
			if code != nil {
				logrus.Infoln("Commit", code)
				if *commitCfg.Version >= 0 {
					logrus.Infoln("手工指定version为", *approveCfg.version)
					code.Version = uint64(*approveCfg.version)
				}
				err = app.Commit(cfg.GetBase(), code, *commitCfg.Force)
			} else {
				err = errors.New("没有找到这个package：" + args[0])
			}
		} else {
			err = app.Commit(cfg.GetBase(), nil, *commitCfg.Force)
		}
		checkErr(err)
	},
}

func init() {
	rootCmd.AddCommand(commitCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// commitCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	commitCfg.Version = commitCmd.Flags().Int64P("version", "V", -1, "指定版本号")
	commitCfg.Force = commitCmd.Flags().BoolP("force", "f", false, "指定版本号")
}
