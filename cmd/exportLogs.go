/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"strings"
	"time"

	"gitee.com/extrame/fb-runner/docker"
	"gitee.com/extrame/fb-runner/store"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// exportLogsCmd represents the exportLogs command
var exportLogsCmd = &cobra.Command{
	Use:   "exportLogs",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		var reget = false
		if len(args) > 1 {
			reget = args[1] == "--restart"
		}
		services, err := docker.Services(cfg.GetSource(), cfg.GetBase(), cfg.NetwokName, strings.ToLower(cfg.Database) == "couchdb", cfg.Ca)
		var logF = cfg.GetLogFile()
		var endAt time.Time
		if err == nil {
			var date = store.GetLastExportDate()
			for _, s := range services {
				if reget {
					date = time.Time{}
				}
				endAt, _ = docker.ExportLog(s, date, logF)
			}
		}
		if err == nil {
			err = store.SetLastExportDate(endAt)
		}
		if err != nil {
			logrus.Fatalln(err)
		}
	},
}

func init() {
	rootCmd.AddCommand(exportLogsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// exportLogsCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// exportLogsCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
