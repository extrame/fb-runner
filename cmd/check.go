/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"

	"gitee.com/extrame/fb-runner/app"
	"github.com/spf13/cobra"
)

// checkapproveCmd represents the checkapprove command
var checkapproveCmd = &cobra.Command{
	Use:   "check",
	Short: "检查approve/commit状态",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			cmd.PrintErr("请指定是check approve还是commit")
		}
		var channel, name string
		if len(args) > 1 {
			channel = args[1]
		}
		if len(args) > 2 {
			name = args[2]
		}
		var err error
		switch args[0] {
		case "approve":
			err = app.CheckApprove(cfg.GetBase(), "approved", channel, name)
		case "commit":
			err = app.CheckApprove(cfg.GetBase(), "committed", channel, name)
		}
		fmt.Println(err)
	},
}

func init() {
	rootCmd.AddCommand(checkapproveCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// checkapproveCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// checkapproveCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
