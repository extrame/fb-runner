/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"gitee.com/extrame/fb-runner/channel"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// createCmd represents the create command
var createCmd = &cobra.Command{
	Use:   "create",
	Short: "创建链码",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		var action = "channel"
		if len(args) == 0 {
			logrus.Fatalln("need type info for create, such as 'create channel', if no type info is specified, default action is creating channel, so you can input 'create myChannel' to create channel named myChannel instand of 'create channel myChannel'")
		} else {
			action = args[0]
		}
		var name string
		switch action {
		case "channel":
			if len(args) == 1 {
				logrus.Fatalln("need type info for create, such as 'create channel', if no type info is specified, default action is creating channel, so you can input 'create myChannel' to create channel named myChannel instand of 'create channel myChannel'")
			}
			name = args[1]
		default:
			name = args[0]
		}
		createChannel(name)
		logrus.Infoln("Creating channel '" + name + "'.")
		logrus.Infoln("If network is not up, starting nodes with CLI timeout of '${MAX_RETRY}' tries and CLI delay of '${CLI_DELAY}' seconds and using database '${DATABASE} ${CRYPTO_MODE}")
	},
}

func init() {
	rootCmd.AddCommand(createCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// createCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// createCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func createChannel(name string) {

	err := channel.Create(cfg.GetBase(), name)
	checkErr(err)
}
