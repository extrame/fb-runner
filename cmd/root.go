/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"

	"gitee.com/extrame/fb-runner/docker"
	"gitee.com/extrame/fb-runner/store"
	"gitee.com/extrame/fb-runner/util"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"github.com/spf13/viper"
)

var cfgFile string
var cfg = util.Config

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "fb-runner",
	Short: "Fabric测试环节执行器",
	Long: `Fabric测试环节执行器。
	
	执行包括：up，down、create等`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig, checkCfg)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "配置文件 (默认为 <<执行文件目录>>/runner.yaml)")

}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {

		// Search config in home directory with name ".fb-runner" (without extension).
		viper.AddConfigPath(util.GetExecDir())
		viper.SetConfigType("yaml")
		viper.SetConfigName("runner")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	err := viper.ReadInConfig()
	if err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
		var f *os.File
		f, err = os.Open(viper.ConfigFileUsed())

		if err == nil {
			err = yaml.NewDecoder(f).Decode(cfg)
		}
	}
	cobra.CheckErr(err)
}

func checkCfg() {
	if cfg.Init() {
		fw, err := os.OpenFile(viper.ConfigFileUsed(), os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0666)
		if err != nil {
			logrus.Fatalln("open config for write", err)
		}
		err = yaml.NewEncoder(fw).Encode(cfg)
		if err != nil {
			logrus.Fatalln(err)
		}
		fw.Close()
	}
	store.Init(util.GetExecDir())
	err := docker.Init()
	cobra.CheckErr(err)

	err = cfg.CheckSource()
	cobra.CheckErr(err)

}
