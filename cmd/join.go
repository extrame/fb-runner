/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"gitee.com/extrame/fb-runner/channel"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var joinCfg struct {
	block *string
}

// joinCmd represents the join command
var joinCmd = &cobra.Command{
	Use:   "join",
	Short: "加入某个已存在的channel",
	Long:  `输入Channel名称`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) > 0 {
			err := channel.JoinChannel(cfg.GetBase(), args[0])
			cobra.CheckErr(err)
		} else {
			logrus.Fatalln("请输入Channel名称")
		}
	},
}

func init() {
	rootCmd.AddCommand(joinCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// joinCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	joinCfg.block = joinCmd.Flags().StringP("blockpath", "b", "", "Path to file containing genesis block")
}
