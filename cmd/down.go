/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"gitee.com/extrame/fb-runner/docker"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// downCmd represents the down command
var downCmd = &cobra.Command{
	Use:   "down",
	Short: "停止运行",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		logrus.Infoln("Stopping network")
		forceRemove, _ := cmd.Flags().GetBool("remove-data")
		networkDown(forceRemove)
	},
}

func init() {
	rootCmd.AddCommand(downCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// downCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	downCmd.Flags().Bool("remove-data", false, "去除保存数据")
}

func networkDown(forceRemove bool) {
	if forceRemove {
		logrus.Errorln("")
		scanner := bufio.NewScanner(os.Stdin)
		fmt.Print("正在使用强制删除数据指令,是否继续\n")
		fmt.Print("    是（Y）/否（N）:")
		if scanner.Scan() {
			var answer = scanner.Text()
			if strings.ToLower(answer) != "y" && answer != "是" {
				forceRemove = false
				logrus.Infoln("取消强制删除")
			}
		}
	}
	docker.Stop(cfg.GetBase(), cfg.NetwokName, forceRemove || !cfg.Persist)
}
