package util

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func AskForConfirmation(s string) bool {
	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Printf("%s [y/n]: ", s)

		response, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}

		response = strings.ToLower(strings.TrimSpace(response))

		if response == "y" || response == "yes" || response == "是" {
			return true
		} else if response == "n" || response == "no" || response == "否" {
			return false
		}
	}
}

func GetAnswer(s string) string {
	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Print(s)

		response, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}

		return strings.Trim(response, " \n")
	}
}
