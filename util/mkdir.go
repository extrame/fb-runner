package util

import (
	"os"
	"path/filepath"
)

func MkdirFor(name string) error {
	dir := filepath.Dir(name)
	return os.MkdirAll(dir, 0700)
}

func Mkdir(name string) error {
	return os.MkdirAll(name, 0700)
}
