package util

import "testing"

func TestGetName(t *testing.T) {
	var o = Organization{
		key: "test",
	}
	if o.GetName() != "Test" {
		t.Fatal("get name wrong,should be Test, but get " + o.GetName())
	}
	o.key = ""
	if o.GetName() != "" {
		t.Fatal("get name wrong,should be nothing, but get " + o.GetName())
	}
}
