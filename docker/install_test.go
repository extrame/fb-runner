package docker

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"testing"
)

func TestInstall(t *testing.T) {
	err := Install("hyperledger/fabric-baseos", "2.3.2")
	fmt.Println(err)
}

func TestHexSha256(t *testing.T) {
	var s = sha256.New()
	s.Write([]byte("hello333435312342faeff"))
	var sum = s.Sum(nil)
	fmt.Println(sum)
	fmt.Println(hex.EncodeToString(sum))
}

func TestWriteTar(t *testing.T) {
	err := writeDirToTar("tmp_hyperledger/fabric-baseos_2.3.2", "fabric-baseos_2.3.2_img.tar")
	fmt.Println(err)
}
