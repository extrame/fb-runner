package docker

import (
	"fmt"
	"testing"
	"time"

	"gitee.com/extrame/fb-runner/util"
	"gitee.com/extrame/fb-runner/version"
)

func TestCopyTemplate(t *testing.T) {
	util.Config.Init()
	util.Mkdir("../test/result/docker")
	err := copyTemplate(version.New("Version: 2.3.0"), "../samples", "../test/result/", "net", "docker/docker-compose-test-net.yaml")
	fmt.Println(err)
}

func TestCopyTemplateOfCouch(t *testing.T) {
	util.Config.Init()
	util.Mkdir("../test/result/docker")
	err := copyTemplate(version.New("Version: 2.3.0"), "../samples", "../test/result/", "net", "docker/docker-compose-couch.yaml")
	fmt.Println(err)
}

func TestParseTime(t *testing.T) {
	ti, err := time.Parse("2006-01-02 15:04:05 -0700 MST", "2021-09-17 12:10:15 +0800 CST")
	fmt.Println(ti, err)
}
