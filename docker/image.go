package docker

// /{"Containers":"N/A","CreatedAt":"2020-07-09 21:28:39 +0800 CST","CreatedSince":"11 months ago","Digest":"\u003cnone\u003e","ID":"5eb2356665e7",
// "Repository":"hyperledger/fabric-tools","SharedSize":"N/A","Size":"519MB","Tag":"2.2.0","UniqueSize":"N/A","VirtualSize":"518.8MB"}
type Image struct {
	Containers   string
	CreatedAt    string
	CreatedSince string
	Digest       string
	ID           string
	Repository   string
	SharedSize   string
	Size         string
	Tag          string
	UniqueSize   string
	VirtualSize  string
}
