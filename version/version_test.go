package version

import (
	"fmt"
	"testing"
)

func TestParse(t *testing.T) {
	var tested = `peer:
Version: 2.2.0
Commit SHA: 5ea85bc54
Go version: go1.14.4
OS/Arch: linux/amd64
Chaincode:
 Base Docker Label: org.hyperledger.fabric
 Docker Namespace: hyperledger`
	var v = New(tested)
	fmt.Println(v)
}

func TestUnsupported(t *testing.T) {
	fmt.Println(unSupported.MatchString("1.1.2"))
	fmt.Println(unSupported.MatchString("2.3.0"))
}
