package version

import (
	"regexp"
)

type Version string

var Finder = regexp.MustCompile("Version:\\s+(\\S*)")
var unSupported = regexp.MustCompile("^1\\.[0-4]\\.\\S+$")

func New(s string) Version {
	finded := Finder.FindAllStringSubmatch(s, -1)
	if len(finded) > 0 {
		if len(finded[0]) > 1 {
			return Version(finded[0][1])
		}
	}
	return Version("")
}

func (v *Version) IsSupported() bool {
	return true //"^1\.0\. ^1\.1\. ^1\.2\. ^1\.3\. ^1\.4\."
}

func (v *Version) String() string {
	return string(*v)
}
